<?php
class animal {
    public $name,
           $legs,
           $cold_blooded;



    public function __construct($name = "name", $legs = 4, $cold_blooded = "no"){
        $this->name=$name;
        $this->legs=$legs;
        $this->cold_blooded=$cold_blooded;
    }

    public  function get(){
        $str = "Name : {$this->name} <br>Legs : {$this->legs} <br>Cold Blooded : {$this->cold_blooded}";
        return $str;
    }
}