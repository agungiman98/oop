<?php

class ape extends animal {
    public $yell;
    
    public function __construct($name = "name", $legs = 4, $cold_blooded = "no", $yell= "auoo"){
        parent::__construct($name, $legs, $cold_blooded);
        $this->yell = $yell;
    }

    public function get(){
        $str = parent::get() . "<br>Yell : {$this->yell}";
        return $str;
    }
}