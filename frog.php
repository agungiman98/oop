<?php

class frog extends animal {
    public $jump;
    
    public function __construct($name = "name", $legs = 4, $cold_blooded = "no", $jump= "Hop Hop"){
        parent::__construct($name, $legs, $cold_blooded);
        $this->jump = $jump;
    }

    public function get(){
        $str = parent::get() . "<br>Jump : {$this->jump}";
        return $str;
    }
}